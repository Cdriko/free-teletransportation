/*
http://heloisaneves.com/2013/03/12/fab-teletransportation/



Free teletransportation
version puredata/openscad interface
by cedric doutriaux
creative comons cc-by-sa
7-2013


activate design>Automatic reload and compile
*/


include <bezier.scad>
include <ftt.params.scad>

if(sinAsa){

}else{
echo("anse");
anse();
}





rotate_extrude(convexity = 10)
union(){
	//main body
	BezCubicRibbon([[R+e,0],[R+e,Bf],[R+e+d,H+Bb],[R+e+d,H]], [[R,0],[R,Bf],[R+d,H+Bb],[R+d,H]]);
	//fondo
	BezCubicRibbon([[R+e,0],[R+e,h],[R+e,h+e],[0,h+e]],[[R,0],[R,h],[R,h],[0,h]]);

}



module anse(){
	//calculs pour la anse
	
	
	
	asaBegin=PointOnBezCubic2D([R+e,0],[R+e,Bf],[R+e+d,H+Bb],[R+e+d,H], Ba);
	asaEnd=PointOnBezCubic2D([R+e,0],[R+e,Bf],[R+e+d,H+Bb],[R+e+d,H], Ta);
	
	
	
	a1=asaBegin-[0.5,0];
	a2=asaBegin+[Bs+r,0];
	a3=asaEnd+[Bi+r,0];
	a4=asaEnd+[-0.5,0];
	
	b1=a1-[0,r];
	b2=a2+[-r,-r];
	b3=a3+[-r,r];
	b4=a4+[-0.5,r];
	
	/*
	//for debug
	translate(a1)cube(2,center=true);
	translate(a2)cube(2,center=true);
	translate(a3)cube(2,center=true);
	translate(a4)cube(2,center=true);
	
	translate(b1)cube(2,center=true);
	translate(b2)cube(2,center=true);
	translate(b3)cube(2,center=true);
	translate(b4)cube(2,center=true);
	*/
	
	//asa
	rotate([90,0,0])
	linear_extrude(height = r, convexity = 10) 
			BezCubicRibbon(
	[
	a1,a2,a3,a4
	],[
	b1,b2,b3,b4
	]);
}



